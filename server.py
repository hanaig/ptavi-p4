# !/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import socketserver
import sys
import json
import time

puerto = int(sys.argv[1])


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Echo server class """
    diccionario = {}

    def meterUsuario(self, dirSip1, tiempoExpires):
        """ registrar usuarios en el diccionario """
        ipClient, puertoClient = self.client_address
        self.diccionario[dirSip1] = {'address': ipClient,
                                     'expires': tiempoExpires}
        print(self.diccionario)
        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')

    def borrarUsuario(self, dirSip2):
        """ borrar usuarios del diccionario """
        try:
            del self.diccionario[dirSip2]
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        except KeyError:
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        print(self.diccionario)

    def register2json(self):
        """ crear el archivo Json """
        with open("registered.json", 'w') as f:
            json.dump(self.diccionario, f, indent=2)

    def caducidadExpires(self):
        """ comprobar el tiempo expires de los usuarios """
        listaUsuarios = list(self.diccionario)
        for usuario in listaUsuarios:
            tiempoExpires = self.diccionario[usuario]["expires"]
            tiempoReal = time.strftime('%Y-%m-%d %H:%M:%S +0000',
                                       time.localtime())
            if tiempoExpires < tiempoReal:
                del self.diccionario[usuario]
        self.register2json()

    def json2registered(self):
        """ comporobar si exixte fichero json """
        try:
            with open('registered.json', 'r') as ficheroJson:
                self.diccionario = json.load(ficheroJson)
        except FileNotFoundError:
            pass

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        global dirSip
        self.json2registered()
        for line in self.rfile:
            mensajeClient = line.decode('utf-8')
            if mensajeClient != '\r\n':
                print("El cliente nos manda ", mensajeClient)
                datosCliente = mensajeClient.split()
                if datosCliente[0] == 'REGISTER':
                    dirSip = datosCliente[1].split(':')[1]
                elif datosCliente[-2] == 'Expires:':
                    tiempoExpires = int(datosCliente[-1])

                    if tiempoExpires == 0:
                        self.borrarUsuario(dirSip)
                    elif tiempoExpires > 0:
                        tiempoExpires = tiempoExpires + time.time()
                        tiempoExpires = time.strftime(
                            '%Y-%m-%d %H:%M:%S +0000',
                            time.localtime(tiempoExpires))
                        self.meterUsuario(dirSip, tiempoExpires)
                    self.caducidadExpires()


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', puerto), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
